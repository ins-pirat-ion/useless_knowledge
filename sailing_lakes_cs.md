Každý s sebou
=============

* občanky/pasy
* řidičák
* prachy - CZK, PLN, na kauci
  * zkusenost Mazury 2019 - 1 jidlo (pol.,ryba, pivo) cca 60-80 PLN
  * pristavy na noc >50, na den kempy s kadibudkou 30
  * parking 80 PLN
  * benzin do lodi cca 50 PLN
  * 2020 jsem si krom doplatku bral +500 PLN, v srpnu nezbylo nic, v rijnu 200 ;)
* platební karty
* mapy
  * automapy
  * jezera

* spacák nebo peřina+prostěradlo
* polštářek (hodí se i na cestu)
* čelovka
* zavírák
* sluneční brejle (klapky)
* gumu na brejle

* kapesníky
* papír. kapesníky
* kartáček, zubní nit
* pasta
* ručník
* mýdlo/sprcháč
* opalovací krém
* léky

* kšiltovka/námořnická čepicka
* zmijovka
* rukavice pracovní
* plástenka - vyzkoušená je souprava kalhoty+bunda z pracovních potřeb asi za 250,-
* větrovka
* oblečení do vedra, zimy, větru, deště...
* kalhoty dlouhé
* kraťasy
* fusekle
* trenky
* trika s kratkým (pruhovaný)
* trika s dlouhým (pruhovaný)
* boty na pevninu
* boty se světlou podrážkou na palubu (dá se i naboso na vlastní nebezpečí)
* teplá bunda
* mikyna
* pokud máte dobré dětske vesty, lepší vzít své
* holínky se světlou podrážkou dle počasí zateplené
  (!!! opravdu si holínky vemte, zkuste jít v noci do bažiny za neodbytnými účely v něčem jiném ;) )

* energydrinky na cestu

* hudební nástroje, zpěvníky
* učebnice a povinná četba :)
* hudební nosiče

* pivo (6 plechovek na den min)
* portský
* rum (2 lahve min)

1x Na loď
=========

* skipper průkaz VMP (kapitán)
* smlouva (kapitán)
* karimatky 2
* společ. hry
  * amos
  * karty
  * Panic Lab
  * bang
* pytle na odpadky
* utěrky
* houbičky
* jar odbouratelný
* vařečka
* struhadlo
* větší prkýnko
* pánev
* větší hrnec
* sirky 3x, zapalovač, svicky, jehla, nit, panapr
* kolíčky
* šňůrky 1m, delší šňůra
* ducktape
* křížovej šroubovák
* nepotřebná audiokazeta či bavlnka zelená, červená na špiónky, nebejvaj :(
* moskytiéra
* teplometná kostička (dle počasí)
* lopatka na le kaků
* na Orlík velkou houbu a rejžák ;)

* med
* cukr
* sůl
* čaje
* kafe
* airpress/mokkátor
* olej
* tuzemák do čaje

* tento seznam a pravidla ;)

Při sestavování posádky
=======================

* nastavení očekávání
  * yachting není dovolená :)
  * jízda a program se přizpůsobuje počasí, větru, nikoli naopak
  * dobrý vítr je vzácný, tj. koupání je za bezvětří, fouká-li, jede se na plachty
  * motor se používá v nouzi, přednost mají noční plavby (moře)
  * bezpečnost má přednost před pohodlím a zábavou
  * přeplavby mohou být dlouhé, včetně nočních plaveb (moře)
  * specifika záchodů (na Mazurech na lodi nejsou, na moři ventily)

Před vyplutím
=============

* zjistit, kdo ma zájem být kompetentní posádkou
* uzly - loďák, fendry, roháček, držení, dotahování přes roháček, vázání ke sloupu, liščák, osma s okem, smotání lana
* kde co je
  * lana v zásecích - které co
  * výtah hlavní plachty
  * rolfok
  * otěže hlavatky
  * otěže kosatky
  * kačena
  * kicking
  * vinčny
  * kotva, ovladač
* naplánovat podle mapy a průvodce trasu, nespěchat při vyplutí
* připravit kormidlo k rychlému spuštění
* kačenu dolů

Pravidla
========

* pravidlo č. 1: kapitán má vždycky pravdu :)
* kapitán zodpovídá za bezpečnost a pokyny kapitána se plní okamžitě, bez diskusí
* na kapitánský můstek nikdo krom kapitána nic neodkládá
* nestojí se na schůdcích do kajuty

Zkušenosti
==========

* přebírání
  * stav kormidla
  * stav kloubu rahna
  * stav zvedáku rolfoku
  * zadní utahovák líku kosatky
* v chladných měsících se hodí topení
* nestrkat spotřebiče do orosené zásuvky
* bacha při zvedání stěžně - lanka za vinčnama, kolem kormidelní páky, otěž v patě
