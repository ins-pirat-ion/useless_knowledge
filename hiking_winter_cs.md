Každý s sebou
=============

* občanky/pasy
* řidičák
* jízdenky
* pojištění
* prachy - CZK, EUR
  * zkušenost Tatry 2018 - 1 jidlo s pivem 10 EUR, bus z/do Popradu 2 EUR
  * vlak Poprad - Lipt. Mikuláš 4 EUR
  * pivo v útulně 2 EUR, panák 1.5 EUR
* platební karty

* spacáky 2, klidně oba pořádný
* karimatka nejtlustší
* bačkůrky do spacáku
* čelovka
* zavírák
* sluneční brejle
* sirky 3x, zapalovač, svicky, jehla, nit, panapr
* lékárnička
* termoska se zázvorovým čajem
* lžíce

* kapesníky
* papír. kapesníky
* kartáček, zubní nit
* pasta
* lůj na xicht

* čepice
* kukla
* rukavice
* šála
* zimní bunda
* polarka
* podlikačky
* oteplováky
* návleky
* 3 vrstvy termotrik
* fusekle na každej den
* pořádný zimní pohory / snowborďáky / skialpový
* sněžnice pořádný + fošna + helma / skialpy

* koncovka / flétna / foukačka

* žerky
* fuet
* plátkovej sejra
* buchtičky
* čokolády
* tyčky
* hroznovej cukr
* zalívací polívky
* litr guarana kofoly
* 1-2 1.5 litrový flašky na vodu
* placatka s kořalkou

Do dvojice
=========

* vařič
* bomby do zimy
* ešus

* cukr
* sůl
* čaje

Do čtveřice
===========

* stan
* mapy hor

Zkušenosti
==========

* vodu nabrat včas, studánky vysoko jsou pod sněhem
* nesušit ponožky na nohách ve spacáku, vzít suchý
* tyčinky, hroznovej cukr do kapes, ne do krabice uprostřed batohu
* ~15cm sněhu a jiné stížené podmínky znamenají minimálně 1.5 násobek času oproti mapě
* zjistit alternativy k ubytování, pokud je trasa na hraně
* zjistit autobusy z alternativních vesnic pro případ ústupu z hřebene
* offline mapy do GPS / mobilu
* jízdenky aspoň 14 dní předem, včasná je levnější, lůžka/lehátka jsou brzy pryč
* vlak ve 2 z Olomouce znamená 9:30 na Štrbskym plese - pozdě
* zpátky večerní vlak v sedě, bar v jídeláku celkem levnej ;)
